import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})

export class HeaderComponent implements OnInit
{

  public date: string;

  constructor() { }

  ngOnInit(): void {
    this.date = new Date().toDateString();
  }
}

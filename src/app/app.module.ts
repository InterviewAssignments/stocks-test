import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { StocklistComponent } from './stocklist/stocklist.component';
import { StockListItemComponent } from './stocklist/stocklist-item/stocklist-item.component';

import { HttpClientModule } from '@angular/common/http';
import { StockDataService } from './stock.data.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    StocklistComponent,
    StockListItemComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [StockDataService],
  bootstrap: [AppComponent]
})

export class AppModule { }

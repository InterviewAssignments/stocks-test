export class StockSymbol
{
  symbol: string;
  name: string;
  price: number;
  change: number;

  constructor(symbol: string, name: string, price: number, change: number = 0)
  {
    this.symbol = symbol;
    this.name = name;

    this.price = price;
    this.change = change;
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {StockSymbol} from './models/stocksymbol';

@Injectable()

export class StockDataService
{

  constructor(private http: HttpClient)
  { }
  private apiUrl = 'https://staging-api.brainbase.com/stocks.php';
  private lastInitialRawData;
  private changePercentage = 10;

  private static getRandomInt(min, max): number
  {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  loadSymbols(date: Date, callback): void
  {
    const today = new Date();
    const isToday = (today.toDateString() === date.toDateString());

    if (isToday) {
      this.http.get(encodeURI(this.apiUrl))
        .subscribe( this.onDataReceived.bind(this, (rawData) => {
          this.pipeRawDataToStockSymbolObject(callback, rawData);
        }) );
    } else {
      this.http.get(encodeURI(this.apiUrl))
        .subscribe( this.onDataReceived.bind(this, (rawData) => {
          this.mockSpecifiedDateRawData(callback, rawData);
        }));
    }
  }

  private mockSpecifiedDateRawData(callback, rawData): void
  {
    rawData.map((value) => {
      let amountChange = (value.price / 100) * this.changePercentage;
      amountChange = StockDataService.getRandomInt(amountChange * -1, amountChange);
      value.price = value.price + amountChange;
      value.change = amountChange;
    });

    this.pipeRawDataToStockSymbolObject(callback, rawData);
  }

  private pipeRawDataToStockSymbolObject(callback, rawData): void
  {
    const stockSymbolData = [];
    rawData.forEach((value) => {
      stockSymbolData.push(
        new StockSymbol(value.symbol, value.name, value.price, value.change ? value.change : 0)
      );
    });

    callback(stockSymbolData);
  }

  private onDataReceived(callback, rawData): void
  {
    this.lastInitialRawData = rawData;
    callback(rawData);
  }
}

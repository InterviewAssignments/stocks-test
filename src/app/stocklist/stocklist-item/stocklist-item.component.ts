import {Component, EventEmitter, Input, Output} from '@angular/core';

import { StockSymbol } from '../../models/stocksymbol';

@Component({
  selector: 'app-stocklist-item',
  templateUrl: './stocklist-item.component.html',
})

export class StockListItemComponent
{
  @Input() symbol: StockSymbol;
  @Input() dayNumber: number;
  @Output() symbolSelected = new EventEmitter<StockSymbol>();

  constructor() { }

  getInitialValue(): number
  {
    return this.symbol.price - this.symbol.change;
  }

  onSelected(): void
  {
    this.symbolSelected.emit(this.symbol);
  }
}

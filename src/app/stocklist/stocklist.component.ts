import {Component, EventEmitter, Output, OnInit} from '@angular/core';
import { StockSymbol } from '../models/stocksymbol';
import {StockDataService} from '../stock.data.service';

@Component({
  selector: 'app-stocklist',
  templateUrl: './stocklist.component.html',
})

export class StocklistComponent implements OnInit
{
  // act as the API, or application programming interface, of the child component
  // in that they allow the child to communicate with the parent.
  @Output() selectedStockSymbol = new EventEmitter<StockSymbol>();
  @Output() dateUpdated = new EventEmitter<Date>();

  public symbols: StockSymbol[] = [];
  public todayDate: Date = new Date();

  private dayPassed = 0;

  constructor(private stockData: StockDataService)
  { }

  ngOnInit(): void
  {
    this.updateStockSymbols();
  }

  onStockSymbolSelected(symbol: StockSymbol): void
  {
    this.selectedStockSymbol.emit(symbol);
  }

  updateToNextDay(): void
  {
    this.dayPassed++;
    const nextDay = this.todayDate;
    nextDay.setDate(nextDay.getDate() + this.dayPassed);

    this.dateUpdated.emit(nextDay);
    this.updateStockSymbols(nextDay);
  }

  getCurrentDay(): number
  {
    return this.dayPassed;
  }

  private updateStockSymbols(date = null): void
  {
    this.stockData.loadSymbols(date ? date : this.todayDate, (data: StockSymbol[]) => {
      this.symbols = data;
    });
  }
}

import { Component, ViewChild } from '@angular/core';
import { StockSymbol } from './models/stocksymbol';
import {HeaderComponent} from './header/header.component';

// identifies the class immediately below it as a component class,
// and specifies its metadata.
@Component({
  selector: 'app-chart',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  // makes it possible to access a child component and call methods or access
  // instance variables that are available to the child.
  // component is accessible with this.header
  @ViewChild(HeaderComponent) header: HeaderComponent;

  updateDate(date: Date): void
  {
    this.header.date = date.toDateString();
  }

  symbolSelectedFromList(symbol: StockSymbol): void
  {
    // Symbol selected from list
  }
}
